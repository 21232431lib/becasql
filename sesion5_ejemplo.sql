CREATE TYPE address AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
)

CREATE TYPE person AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    address
)

CREATE TABLE persons OF person

CREATE TYPE PHONE_ARRAY IS VARRAY (10) OF varchar2(30)

CREATE TYPE participant_t AS OBJECT (
  empno   NUMBER(4),
  ename   VARCHAR2(20),
  job     VARCHAR2(12),
  mgr     NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  deptno   NUMBER(2))


CREATE TYPE module_t  AS OBJECT (
  module_id  NUMBER(4),
  module_name VARCHAR2(20), 
  module_owner REF participant_t , 
  module_start_date DATE, 
  module_duration NUMBER )

create TYPE moduletbl_t AS TABLE OF module_t

CREATE TABLE  employees
( empnumber            INTEGER PRIMARY KEY,
  person_data     REF  person,
  manager         REF  person,
  office_addr          address,
  salary               NUMBER,
  phone_nums           phone_array
)

CREATE TABLE projects (
  id NUMBER(4),
  name VARCHAR(30),
  owner REF participant_t,
  start_date DATE,
  duration NUMBER(3),
  modules  moduletbl_t  ) NESTED TABLE modules STORE AS modules_tab ;

CREATE TABLE participants  OF participant_t ;

INSERT INTO persons VALUES (
            person('Wolfgang Amadeus Mozart', 123456,
            address('Am Berg 100', 'Salzburg', 'AU','10424')))
            
INSERT INTO persons VALUES (
            person('Ludwig van Beethoven', 234567,
            address('Rheinallee', 'Bonn', 'DE', '69234')))
            
INSERT INTO employees (empnumber, office_addr, salary, phone_nums) VALUES 
            (1001,
             address('500 Oracle Parkway', 'Redwood City', 'CA', '94065'),
             50000,
             phone_array('(408) 555-1212', '(650) 555-9999'));   
             
UPDATE employees 
  SET manager =  
    (SELECT REF(p) FROM persons p WHERE p.name = 'Wolfgang Amadeus Mozart')
--+++ eSTE NO JALO
UPDATE employees 
  SET person_data =  
    (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven')
INSERT INTO participants VALUES (
participant_T(7369,'ALAN 
SMITH','ANALYST',7902,to_date('17-12-1980','dd-mm-yyyy'),800,20)) ;
INSERT INTO participants VALUES (
participant_t(7499,'ALLEN 
TOWNSEND','ANALYST',7698,to_date('20-2-1981','dd-mm-yyyy'),1600,30));
INSERT INTO participants VALUES (
participant_t(7521,'DAVID 
WARD','MANAGER',7698,to_date('22-2-1981','dd-mm-yyyy'),1250,30));
INSERT INTO participants VALUES (
participant_t(7566,'MATHEW 
JONES','MANAGER',7839,to_date('2-4-1981','dd-mm-yyyy'),2975,20));
INSERT INTO participants VALUES (
participant_t(7654,'JOE 
MARTIN','MANAGER',7698,to_date('28-9-1981','dd-mm-yyyy'),1250,30));
INSERT INTO participants VALUES (
participant_t(7698,'PAUL 
JONES','Director',7839,to_date('1-5-1981','dd-mm-yyyy'),2850,30));
INSERT INTO participants VALUES (
participant_t(7782,'WILLIAM 
CLARK','MANAGER',7839,to_date('9-6-1981','dd-mm-yyyy'),2450,10));
INSERT INTO participants VALUES (
participant_t(7788,'SCOTT 
MANDELSON','ANALYST',7566,to_date('13-JUL-87','dd-mm-yy')-85,3000,20));
INSERT INTO participants VALUES (
participant_t(7839,'TOM 
KING','PRESIDENT',NULL,to_date('17-11-1981','dd-mm-yyyy'),5000,10));
INSERT INTO participants VALUES (
participant_t(7844,'MARY TURNER','SR 
MANAGER',7698,to_date('8-9-1981','dd-mm-yyyy'),1500,30));
INSERT INTO participants VALUES (
participant_t(7876,'JULIE ADAMS','SR ANALYST',7788,to_date('13-JUL-87', 
'dd-mm-yy')-51,1100,20));
INSERT INTO participants VALUES (
participant_t(7900,'PAMELA JAMES','SR 
ANALYST',7698,to_date('3-12-1981','dd-mm-yyyy'),950,30));
INSERT INTO participants VALUES (
participant_t(7902,'ANDY 
FORD','ANALYST',7566,to_date('3-12-1981','dd-mm-yyyy'),3000,20));
INSERT INTO participants VALUES (
participant_t(7934,'CHRIS MILLER','SR 
ANALYST',7782,to_date('23-1-1982','dd-mm-yyyy'),1300,10));

    