1) �Qu� hace la siguiente consulta?
SELECT MAX(salary) "Salario Mayor", MIN(salary) "Salario menor", AVG(salary) "Salario
Promedio", SUM(Salary) "TOTAL Salarios"
FROM employees
WHERE hire_date <= '01/01/04' AND department_id = 50;

R. La consulta despliega el salario maximo, el salirio mini, el promedio y la suma de los salarios del departamento 50, con fecha de contrataci�n menor al dia 01/01/04

2) Realiza una consulta para conocer la comisi�n m�s alta y la comisi�n promedio de los
empleados, pero solamente del departamento 80. Cada Columna debe ir con su respectivo
alias.

R. 
Select MAX(COMMISSION_PCT) AS Maxima comision ,AVG(COMMISSION_PCT) AS Comision promedio
    FROM employees
    WHERE  department_id=80;
    
3) Crea una tabla Estudiantes, con un campo para el nombre, apellido paterno, apellido
materno, edad, y fecha de nacimiento, el campo fecha deber� de tener un valor default null,
y el apellido paterno no deber� permitir valores nulos.
R. 
CREATE TABLE Estudiantes(
ESTUDIANTE_ID NUMBER(6),
NOMBRE VARCHAR(30) ,
APELLIDO_PATERNO VARCHAR(15) NOT NULL,
APELLIDO_MATERNO VARCHAR(15),
EDAD NUMBER(3),
FECHA_NACIMIENTO DATE DEFAULT NULL);

4)�Que hace la siguiente consulta?
CREATE TABLE emp_load (
employee_number CHAR(5),
employee_dob CHAR(20),
employee_last_name CHAR(20),
employee_first_name CHAR(15),
ORGANIZATION EXTERNAL (TYPE ORACLE_LOADER DEFAULT DIRECTORY def_dir1
ACCESS PARAMETERS (RECORDS DELIMITED BY NEWLINE FIELDS
(employee_number CHAR(2), employee_dob CHAR(20), employee_last_name CHAR(18),
employee_first_name CHAR(11), "mm/dd/yyyy")) LOCATION ('info.dat'));
R. 
Se crea una tabla emp_load externa donde la informaci�n de la tabla esta localizada en el archivo info.dat 

5) �Cu�l es la diferencia entre ambos registros?

09-JUL-2020 05.42.30.000000 PM
09-JUL-2020 05.42.30.567945 PM

R.La diferencia son los digitos para las fracciones de segundo permitidos 

6) Escribe el c�digo para agregar una columna comida_favorita a la tabla Estudiantes con
valor default null
R:
ALTER TABLE Estudiantes
ADD (COMIDA_FAVORITA VARCHAR(30) DEFAULT NULL);


7) �Qu� hace la siguiente l�nea?
ALTER TABLE nombre_tabla 
SET UNUSED nombre_columna;
R. Alatera la tabla nombre_tabla configurando la columna nombre_columna como no usada, asi cuando se haga una consulta a la tabla
esta columna ya no aparecera

8) Muestra en una consulta el nombre, apellido, nombre de departamento de todos los
empleados, si no tiene departamento asignado muestra "No asignado".

SELECT E.FIRST_NAME,E.LAST_NAME, NVL(D.DEPARTMENT_NAME,'NO ASIGNADO')
FROM EMPLOYEES E LEFT OUTER JOIN DEPARTMENTS D 
ON (E.DEPARTMENT_ID=D.DEPARTMENT_ID);

9) Muestra nombre, apellido, empleo actual, puestos anteriores y fecha de fin de cada
puesto para todos los empleados que fueron contratados entre 2003 y 2005.
R:

SELECT E.FIRST_NAME,E.LAST_NAME, E.JOB_ID, J.JOB_ID, J.END_DATE
FROM HR.EMPLOYEES E LEFT OUTER JOIN HR.JOB_HISTORY J 
ON (E.EMPLOYEE_ID=J.EMPLOYEE_ID)
WHERE EXTRACT(YEAR FROM E.HIRE_DATE) BETWEEN 2003 AND 2005;

10) MUESTRA EL ID DE TRABAJADOR, LAST NAME, ID DE MANAGER ORDENADOS POR ID
DE MANAGER (selfjoin)
R:
NO ENTENDI BIEN LA PREGUNTA PERO HICE DOS MANERAS

SELECT EMPLOYEE_ID,LAST_NAME, MANAGER_ID
FROM HR.EMPLOYEES
ORDER BY MANAGER_ID;

SELECT E.EMPLOYEE_ID,E.LAST_NAME,EM.MANAGER_ID AS MANAGER, EM.LAST_NAME
FROM HR.EMPLOYEES E JOIN HR.EMPLOYEES EM 
ON(E.EMPLOYEE_ID= EM.EMPLOYEE_ID)
ORDER BY EM.MANAGER_ID;


11) MUESTRA ID DE TRABAJADOR, LAST NAME, ID DE MANAGER, LAST NAME DE
MANAGER CON UN QUERY DE JERARQUIA (El primer eslabon es el employee_id=100) Y
ORDENARLOS POR EL ID DEL MANAGER (join de jerarquia)
R.
Me cuesta entender la redaccion 

SELECT EMPLOYEE_ID,LAST_NAME,MANAGER_ID ,(SELECT LAST_NAME
                                                            FROM HR.EMPLOYEES
                                                            WHERE EMPLOYEE_ID=100)
FROM HR.EMPLOYEES
ORDER BY EMPLOYEE_ID;



12) Explica lo que realiza la siguiente sentencia:
SELECT AVG (DISTINCT salary)
FROM employees
WHERE department_id = 30;

R. Saca el promedio de los salarios del departamento 30 que no se repiten

13) Escribe la sentencia donde se muestre cu�ntos sueldos diferentes se pagan a los
empleados del departamento 50 de tabla empleados.

SELECT COUNT(DISTINCT SALARY)
FROM EMPLOYEES
WHERE DEPARTMENT_ID=30

14) Escribe la sentencia para obtener el nombre, apellido, el puesto y el salario de los
empleados que tienen un salario m�nimo mayor a $5,000, y ord�nalo por salario.

SELECT FIRST_NAME, LAST_NAME, JOB_TITLE, SALARY
FROM HR.EMPLOYEES NATURAL JOIN HR.JOBS
WHERE MIN_SALARY > 5000
ORDER BY SALARY


15) Describe lo que realiza la siguiente sentencia.
SELECT department_name, street_address, country_name
FROM departments JOIN locations USING (location_id) JOIN countries USING (country_id)
ORDER BY department_name;

R. Despliega el nombre del departamento , la localizacion del departamento y el pais donde se encuentra el departamento y 
los ordena por nombre de departamento


16) Se requiere una tabla para realizar el registro de inscripci�n de participantes a un
concurso, donde se requiere un numero de boleto que ser� �nico y se registraran el nombre
y apellido, hasta un m�ximo de 50 participantes. Crear la tabla, la secuencia y agregar al
menos 2 participantes.

R. 
CREATE TABLE PARTICIPANTES
(BOLETO_ID NUMBER(2,0) CONSTRAINT PARTICIPANTES_BOLETO_ID_UK UNIQUE , 
NOMBRE VARCHAR(30) NOT NULL,
APELLIDO VARCHAR(30)NOT NULL);
CREATE SEQUENCE BOLETO_SEQ INCREMENT BY 1 START WITH 1 MAXVALUE 50 NOCACHE NOCYCLE;

INSERT INTO PARTICIPANTES VALUES (BOLETO_SEQ.NEXTVAL,'sTEPHANY','HERRERA');
INSERT INTO PARTICIPANTES VALUES (BOLETO_SEQ.NEXTVAL,'CRISTHIAN','FLORES');


17) Explica la siguiente l�nea de SQL:
CREATE SYNONYM respaldo 
FOR copy_employees;

R. CREA UN SINONIMO PARA LA TABLA COPY_EMPLOYEES;


18) Define una tabla con el nombre "estudiantes", con los siguientes campos y sus
caracter�sticas:
1.* "estudiante_id" -> con longitud de 5 y que no sea NULL
2.* "estudiante_nombre" -> con longitud de 20 y que no sea NULL
3.* "estudiante_apellido_paterno" -> con longitud de 20 y que permita valores NULL
4.* "estudiante_apellido_materno" -> con longitud de 20 y que permita valores NULL
5.* "estudiante_numero" -> con longitud de 15 que permita valores NULL
Y que contenga un CONSTRAINT con el nombre "uk_estudiantes" a nivel tabla en donde se
indique que el "estudiante_apellido_paterno" y "estudiante_apellido_materno" tienen que
ser �nicos en toda la tabla, haciendo uso de la keyword UNIQUE y mostrar que
efectivamente se cre� que CONSTRAINT para la tabla.

CREATE OR REPLACE TABLE ESTUDIANTES
(estudiante_id NUMBER(5,0) NOT NULL,
estudiante_nombre VARCHAR(20) NOT NULL,
estudiante_apellido_paterno VARCHAR(20),
estudiante_apellido_materno VARCHAR(20),
estudiante_numero  NUMBER(15,0),
CONSTRAINT uk_estudiantes UNIQUE (estudiante_apellido_paterno,estudiante_apellido_materno));

SELECT *
FROM USER_CONSTRAINTS WHERE TABLE_NAME='ESTUDIANTES';


19) Crea una tabla llamada "jefes" con un id, una fecha de contrataci�n y de finalizaci�n, el
id de la escuela y el id regional de la escuela, agregando un CONSTRAINT a la
fecha_contratacion para que no sea NULL, crea un CONSTRAINT a nivel tabla donde se
define una PRIMARY KEY compuesta por dos campos y al final compara que la fecha de
finalizaci�n sea mayor a la de contrataci�n
R:

CREATE TABLE JEFES
(ID NUMBER(2),
FECHA_CONTRATACION DATE CONSTRAINT JEFES_FECHA_CONTRATACION NOT NULL,
FINAL_CONTRATACION DATE,
ESCUELA_ID NUMER (4),
REGIONAL_ID (5),
CONSTRAINT JEFES_PK PRIMARY KEY(FECHA_CONTRATACION,ID NUMBER),
CONSTRAINT JEFES_CHECK CHECK (FINAL_CONTRATACION>FECHA_CONTRATACION));


20) Consulta el apellido, convi�rtelo a may�sculas y reemplaza las A may�sculas por -,
concat�nalos con el nombre en min�sculas, usa la tabla de employees.
R:
SELECT (REPLACE(UPPER(LAST_NAME),'A','-')) || LOWER(FIRST_NAME)
FROM EMPLOYEES;


21) �Qu� hace la siguiente consulta?
SELECT department_id, manager_id, AVG(salary)
FROM employees
GROUP BY ROLLUP (department_id, manager_id);
R. MUESTRA EL DEPARTMENT_ID, EL MANEGAR_ID Y EL PROMEDIO DE LOS SALARIO Y LOS AGRUPA  POR DEPARTMANETO Y DESPUES POR MANAGER

22) Se desea generar una vista que contenga los siguientes datos: -first_name, last_name,
employee_id y salary y que sea con los registros que tengan un salario entre $17,000 y
$3,200
R:
CREAT VIEW VIEW_SALARY AS SELECT first_name,last_name, employee_id , salary 
FROM EMPLOYEES
WHERE SALARY BETWEEN 17000 AND 3200;

SELECT*
FROM VIEW_SALARY 
ORDER BY SALARY 

23) �Qu� datos se obtienen de hacer la siguiente uni�n de tablas?

SELECT employees.last_name, departments.department_name
FROM HR.employees, HR.departments;
R. SE OBTINENE LOS REGISTRO DE AMBAS TABLAS MENOS LOS QUE SE REPITEN

24) Considerando una lista de empleados y una lista de departamentos. �De qu� forma es
posible visualizar si cada empleado est� asignado o no a un departamento?
R:
SELECT e.last_name, d.department_name
FROM employees e, departments d
WHERE e.department_id = d.department_id(+);


25) �Qu� hace la siguiente consulta?
SELECT LAST_DAY( (SELECT MAX(HIRE_DATE)
FROM HR.EMPLOYEES
WHERE HIRE_DATE<SYSDATE))
FROM DUAL;
R.
MUESTRA EL ULTIMO DIA DEL MES DE LA MAXIMA FECHA DE CONTRATACION, MENOR AL DIA ACTUAL

26) �Qu� hace la siguiente consulta?
SELECT HIRE_DATE, EMPLOYEE_ID, JOB_ID, TO_DATE(NULL)
FROM EMPLOYEES
WHERE EMPLOYEE_ID>(SELECT MIN(EMPLOYEE_ID) FROM employees)
UNION SELECT START_DATE,EMPLOYEE_ID,JOB_ID, END_DATE
FROM JOB_HISTORY
WHERE JOB_ID < (SELECT MAX(JOB_ID) FROM JOB_HISTORY)
ORDER BY EMPLOYEE_ID;

BUSCA EL EMPLOYEE_ID MIN DE LA TABLA EMPLOYEES, BUSCA EL JOB_ID MAXIMO Y DE ACUERDO A ESOS RESULTADOS UNE LAS TABLAS EMPLOYEES Y JOB_HISTORY

27) Dadas las tablas Job_history y jobs, encuentre los Job_id que se intersecan.
R:
SELECT JOB_ID
FROM JOB_HISTORY 
INTERSECT
SELECT JOB_ID
FROM JOBS


28) Si quiero saber los nombres de empleados que fueron contratados despu�s de Peter
Vargas y no tengo la informaci�n de la fecha de contrataci�n de Peter, �C�mo realizar�a esa
consulta? , escribe el c�digo:
R:
SELECT FIRST_NAME, LAST_NAME
FROM EMPLOYEES
WHERE HIRE_DATE>(SELECT HIRE_DATE
                FROM EMPLOYEES
                WHERE FIRST_NAME='PETER' AND LAST_NAME='VARGAS')


29)�Qu� realiza la siguiente consulta?
SELECT last_name, salary
FROM employees
WHERE salary < (SELECT AVG(salary) FROM employees);
R.
DESPLIEGA EL APELLITO Y SALARIO DE LOS EMPLEADOS QUE TIENEN UN SALARIO MENOR AL PROMEDIO DE LOS SALARIOS

30) �Qu� devuelve la consulta?
SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN
(SELECT EXTRACT(YEAR FROM hire_date) FROM employees WHERE
department_id=90);

R. 
DEVUELVE EL APELLIDO Y FECHA DE CONTRATACION DE LOS EMPLEADOS DEL DEPARTAMENTO 90 SOLO UTILIZANDO EL A�O 

31) �Qu� hace la siguiente consulta?
SELECT last_name, salary, NVL2(commission_pct, salary + (salary * commission_pct),
salary) "Ganancia Total"
FROM employees
WHERE department_id IN(80,90);
R. 
DEVUELVE LOS NOMBRES, SALARY Y SUSTITUYE LA COMISION POR SALARY+SALARY*COMISION CUANDO LA COMISION ES NULA 
DE LOS EMPLEADOS DEL DEPARTAMENTO 80 Y 90

32) De la tabla employees, realiza un query que asigne un nombre a un departamento, debe
cumplir con lo siguiente:
Si el campo department_id = 90 asignale 'Gerencia' si el campo department_id = 80 asignale
'Ventas' si el campo department_id = 60 asignale 'Caja' si no pertenece a ninguno asiganale
la palabra OTRO

R. SELECT FIRST_NAME,
CASE DEPARTMENT_ID
WHEN 90 THEN 'GERENCIA'
WHEN 60 THEN 'CAJA'
WHEN 80 THEN 'VENTAS'
ELSE 'OTRO'
END AS 'NOMBRE_DEPARTAMENTO'
FROM EMPLOYEES;


33) �Qu� hace la siguiente consulta?
SELECT department_id, job_id, count(*) AS conteo
FROM HR.employees
WHERE department_id > 10
GROUP BY department_id, job_id
ORDER BY conteo;
R:
MUESTRA EL ID DE DEPARTAMENTO EL JOB_ID Y EL NUMERO DE PERSONAS QUE HAY EN ESE PUESTO APARTIR DEL DEPARTAMENTO 10
AGRUPA LA DE ACUERDO AL DEPARTMEN_ID Y AL JOB_ID

34) Desarrolla el c�digo que agrupe por departamentos de la tabla empleado y obtenga el
salario m�ximo de cada uno de los grupos, a su vez cuente las veces que se repite cada uno
de los departamentos, y �nicamente muestre aquellos datos en el que el conteo fue mayor a
2 empleados por departamento. Finalmente, los datos sean ordenados por el salario m�ximo
de forma ascendente.
R:
SELECT DEPARTMENT_ID,MAX(SALARY),COUNT(*) AS CUENTA
FROM EMPLOYEES
GROUP BY DEPARTMENT_ID HAVING CUENTA>2
ORDER BY MAX(SALARY);


35) �Qu� har� esta sentencia y bajo qu� condiciones?
INSERT ALL
WHEN department_id IN (50) THEN
INTO all_50s VALUES (employee_id, last_name, salary, department_id)
WHEN department_id IN (90)
THEN INTO all_90s VALUES (employee_id, last_name, salary, department_id)
WHEN department_id IN (60)
THEN INTO all_60s VALUES (employee_id, last_name, salary, department_id)

SELECT employee_id, last_name, salary, department_id FROM employees;
R:
VA INSERTANDO LOS EMPLEADOS DE ACUERDO AL NUMERO DE SU DEPARTAMENTO EN SUS TABLAS 
RESPECITVAS (ALL_50S, ALL_60S,ETC) Y AL FINAL MUESTRA LA ID DE EMPLEADO, APELLIDO, SALARIO Y DEPARTMAENTO DE LOS EMPLEADOS

36) �Qu� hace la siguiente sentencia merge?
MERGE INTO backup_employees be USING employees e
ON (be.employee_id = e.employee_id)
WHEN MATCHED THEN UPDATE
SET be.salary = e.salary * 1.5
WHERE e.salary < (SELECT ROUND(AVG(salary))
FROM employees
WHERE department_id = e.department_id)

WHEN NOT MATCHED THEN INSERT
VALUES (e.employee_id, e.first_name, e.last_name, e.email, e.phone_number,
e.hire_date, e.job_id, e.salary, e.commission_pct, e.manager_id, e.department_id);
R:

ESTE QUERY HACE UN AUMENTO EN EL SALARIO DEL 1.5 DE LOS EMPLEADOS QUE TIENEN UN SALARIO MENOR AL SALARIO PROMEDIO











