--10 de julio 
--Sesion 
---++++Diapositivsas 16_2
--INDEX
--Permite hacer consultas mas eficientes, formando patrones de busqueda
CREATE INDEX wf_cont_reg_id_idx 
ON countries(country_id)

CREATE INDEX emps_name_idx 
ON employees(first_name, last_name)

SELECT DISTINCT ic.index_name, ic.column_name, ic.column_position, id.uniqueness 
FROM user_indexes id, user_ind_columns ic 
WHERE id.table_name = ic.table_name AND ic.table_name = 'EMPLOYEES';

--Consulta eficiente con UPPER
SELECT * FROM employees
WHERE UPPER(last_name) = 'KING';

--creacion de un indice por medio de una funcion 
CREATE INDEX upper_last_name_idx
ON employees (UPPER(last_name))

--Esta consulta marca error por que no es del mismo tipo de fecha
SELECT first_name, last_name, hire_date 
FROM employees 
WHERE TO_CHAR(hire_date, 'yyyy') = '1987

CREATE INDEX emp_hire_year_idx 
ON employees (TO_CHAR(hire_date, 'yyyy'))

SELECT first_name, last_name, hire_date 
FROM employees 
WHERE TO_CHAR(hire_date, 'yyyy') = '2004'

--Crear sinonimos para nombres de tablas que son muy largos 
CREATE SYNONYM amy_emps 
FOR amy_copy_employees

--Los sinonimos sirven bastante cuando estas trabajando con diferentes usuarios