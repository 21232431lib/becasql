--Sesion 1

--SELECT
--Regresa todos los registros de una tabla 
SELECT * 
FROM employees

--WHERE
--Devuelve un conjunto de datos con caracteristicas que se especifican en la sentencia WHERE
SELECT first_name, last_name, job_id 
FROM employees 
WHERE job_id = 'SA_REP'


--Cuando se realizan opéraciones aritmeticas es importante tener en cuenta la jerarquia de las operaciones
SELECT last_name, salary, 12*salary +100 
FROM employees;

--ALIAS
-- Darle un ALIAS a una columna, es solo para mostrar a la salida o al despliegue de la tabla
SELECT last_name AS name, commission_pct AS comm 
FROM employees

---+++++++++++++++++++++++++++++++++++++++++++++++
--Diapositivas 2-1

--DESC
--Devuelve las caracteristicas de la tabla, nombre de columnas,tipo de datos, primary and foreign keys, etc
DESC departments

--||
--Concatenacion, une columnas o cadenas de caracteres
SELECT department_id ||' ' || department_name 
FROM departments;

--Hacemos concatenacion y ponemos un alias
SELECT department_id ||' '|| department_name AS " Department Info"
FROM departments;

--Concatenacion de valores fijos
--En la salida se concatenan, columnas y valores numericos fijos
SELECT last_name ||' has a '|| 1 ||' year salary of '|| salary*12 ||    ' dollars.' AS Pay
FROM employees;

--DISTINCT 
--Hace que en la consulta no se muestren los registros duplicados
SELECT DISTINCT department_id 
FROM employees
--++++++++++++++++++++++++++++++++++++++++++++++++
--Diapositivas 2_2

--WHERE
--Ejemplos. Restringen valores de una consulta utilizando where
--Ejemplo 1
SELECT employee_id, first_name, last_name 
FROM employees
WHERE employee_id = 101;
--Ejemplo 2
SELECT employee_id, last_name, department_id 
FROM  employees 
WHERE department_id = 90;
--Ejemplo 3
SELECT first_name, last_name 
FROM employees 
WHERE last_name = 'Taylor';
--++++++++++++++++++++++++++++++++++++++++++
--Diapositivas 2_3
--BETWEEN.. AND
-- Es un operador usado para seleccionar los resgistros basados en un rango de valores especificos
SELECT last_name, salary 
FROM employees 
WHERE salary BETWEEN 9000 AND 11000;

--IN
--Es un operador que permite probar si un registro se encuentra entre determinados valores
SELECT city, state_province, country_id 
FROM locations 
WHERE country_id IN ('UK', 'CA')

--LIKE
--Permite hacer una consulta buscando una caracteristica especifica 
--(%)
--Es un simbolo utilizado para representar una secuencia de zero o otros caracteres
--(_)
--Es un simbolo utilizado para representar un solo caracter
--Ejemplo1 
--Devuelve los empleados que tienen en su last name como segunda letra o
SELECT last_name 
FROM employees 
WHERE last_name LIKE '_o%';

--Ejemplo2
--Cuando la consulta que hacemos tiene un caracter especial como (_), es necesario 
--indicar que se trata de un guion y no de cualquier caracter, para ello utilizamos (\)
SELECT last_name, job_id 
FROM EMPLOYEES
WHERE job_id 
LIKE '%\_R%' ESCAPE '\';

--IS  NULL
--Regresa los valores que son nulos
SELECT last_name, manager_id 
FROM employees 
WHERE manager_id IS NULL;

--IS NOT NULL
--Regresa los valores que no son nulos
SELECT last_name, commission_pct 
FROM employees 
WHERE commission_pct IS NOT NULL
--++++++++++++++++++++++++++++++++++++++++++++++
--Diapositivas 3_1

--AND
--Permite evaluar dos o mas condiciones en una misma consulta
SELECT last_name, department_id, salary 
FROM employees 
WHERE department_id > 50 AND salary > 12000;

--OR
--Permite evaluar una u otra condicion en una misma consulta
SELECT department_name, manager_id, location_id 
FROM departments 
WHERE location_id = 2500 OR manager_id=124;

--NOT
--Muestra los registros que no satisfacen la condicion
SELECT department_name, location_id 
FROM departments
WHERE location_id NOT IN (1700,1800);

--PRECEDENCIA DE OPERACIONES
SELECT last_name||' '||salary*1.05 As "Employee Raise", department_id, first_name 
FROM employees 
WHERE department_id IN(50,80) 
AND first_name LIKE 'C%' OR last_name LIKE '%s%'

--++++++++++++++++++++++++++++++++++++++++++++++++++
--Diapositivas 3_2

--ORDER BY
-- (ASCENDING y DESCENDING)
--Permite ordenar los registro desplegados de acuerdo a algun requesito especifico
SELECT last_name, hire_date 
FROM employees ORDER BY hire_date;
--Ejemplo2
--Cambia el orden del despliegue 
SELECT last_name, hire_date 
FROM employees ORDER BY hire_date DESC

--Ejemplo3 
--Ordena y usa alias
SELECT last_name, hire_date AS "Date Started" 
FROM employees ORDER BY "Date Started"

--ORDEN DE EJECUCION
--FROM
--WHERE
--SELECT
--ORDER BY

--Ejemplo4
--Se puede ordernar tomando mas de un parametro
SELECT department_id, last_name 
FROM employees 
WHERE department_id <= 50 
ORDER BY department_id, last_name

--++++++++++++++++++++++++++++++++++++++++++
--Diapositivas 3_3
INSERT INTO COPY_EMPLOYEES 
VALUES ('PEDRO', 'LOPEZ',)