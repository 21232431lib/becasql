-- Copia tablas 
CREATE TABLE copy_employees AS (SELECT * FROM employees)

CREATE TABLE copy_departments AS (SELECT * FROM departments);

--Inserta registros a una table 
-- Especificando las columnas de la tabla
INSERT INTO copy_departments
(department_id, department_name, manager_id, location_id)
VALUES
(200,'Human Resources', 205, 1500);
-- Inserta registros sin especificar los nombres de las columnas
INSERT INTO copy_departments
VALUES (210,'Estate Management', 102, 1700);

--Insertar registro 
-- en esta marca error por que no se pueden insertar datos nulos
INSERT INTO copy_employees (employee_id, first_name, last_name, phone_number, hire_date, job_id, salary) 
VALUES (302,'Grigorz','Polanski', '8586667641', '15-Jun-2017',  'IT_PROG',4200);

--inserta pero para cambiar el null, inserta cadena vacia
INSERT INTO copy_employees (employee_id, first_name, last_name, email, phone_number,    hire_date, job_id, salary)
VALUES (302,'Grigorz','Polanski', 'gpolanski', '', '15-Jun-2017',   'IT_PROG',4200);
-- inserta un registro usando funcion sysdate y user
INSERT INTO copy_employees (employee_id, first_name, last_name, email, phone_number, hire_date,   job_id, salary)
VALUES (304,'Test',USER, 't_user', 4159982010, SYSDATE, 'ST_CLERK',2500);

--inserta un registro modificando al formato correcto de fecha 
INSERT INTO copy_employees (employee_id, first_name, last_name, email, phone_number, hire_date,   job_id, salary) 
VALUES (301,'Katie','Hernandez', 'khernandez','8586667641', 
TO_DATE('Julio 8, 2017', 'Month fmdd, yyyy'), 'MK_REP',4200);

--inserto registro con formato de hora
INSERT INTO copy_employees (employee_id, first_name, last_name, email, phone_number, hire_date,   job_id, salary) 
VALUES (303,'Angelina','Wright', 'awright','4159982010', 
TO_DATE('Julio 10, 2017 17:20', 'Month fmdd, yyyy HH24:MI'),    'MK_REP', 3600); 

--Inserta varios registro obtenidos de una busqueda, en otra tabla 
INSERT INTO sales_reps(id, name, salary, commission_pct)
SELECT employee_id, last_name, salary, commission_pct, job_id 
FROM   employees
WHERE  job_id LIKE '%REP%'

-- Inserta todos los registros obtenidos de una busqueda en otra tabla
INSERT INTO sales_reps 
SELECT *   
FROM   employees;

--Asigna el salario del empleado 100 al 101
UPDATE copy_employees
SET salary =(SELECT salary 
FROM copy_employees
WHERE employee_id = 100) 
WHERE employee_id = 101;

--asigna salario y job_ id del trabajador 205 al 206
UPDATE copy_employees
SET salary = (SELECT salary 
FROM copy_employees 
WHERE employee_id = 205),job_id = 
(SELECT job_id 
FROM copy_employees
WHERE employee_id = 205)
WHERE employee_id = 206

--Edita una tabla y le a�ade una columna
ALTER TABLE  copy_employees 
ADD (department_name varchar2(30));

--Copia los id de la tabla department a table employees
UPDATE  copy_employees e
SET e.department_name = (SELECT d.department_name 
FROM departments d 
WHERE e.department_id = d.department_id);

--Elimina un registro de la tabla 
DELETE from copy_employees
WHERE employee_id = 303;

--Elimina los managers que tengan menos de dos departamentos
DELETE FROM copy_employees e 
WHERE  e.manager_id IN
(SELECT d.manager_id
FROM employees d 
HAVING count (d.department_id) < 2 
GROUP BY d.manager_id);
