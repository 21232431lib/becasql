--Sesion 2 
-- 7 de julio

--++++++++++++++
--Diapositiva 5_1
--Conversion de datos
--Despliega un tipo fecha, como un char dandole un formato especifico
--ejemplo1
SELECT TO_CHAR(hire_date, 'Month dd, YYYY') 
FROM employees

--ejemplo2
SELECT TO_CHAR(hire_date, 'fmMonth dd, YYYY') 
FROM employees;

--ejemplo3
SELECT TO_CHAR(hire_date, 'fmMonth ddth, YYYY') 
FROM employees

--ejemplo4
SELECT TO_CHAR(hire_date, 'fmDay ddth Mon, YYYY') 
FROM employees

--ejemplo5
SELECT TO_CHAR(hire_date, 'fmDay ddth Mon, YYYY')
FROM employees

--ejemplo6
SELECT TO_CHAR(SYSDATE, 'hh:mm pm') 
FROM dual;

--ejemplo7
SELECT last_name, TO_NUMBER(bonus, '9999') AS "Bonus" 
FROM employees 
WHERE department_id = 80

--ejemplo8
SELECT TO_NUMBER('5,320', '9,999') AS "Number" 
FROM dual