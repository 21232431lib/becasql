--08/julio--
--Sesion 3--


--GROUP BY: permite agrupar en peque�os conjuntos
-- ORDER BY: ordena de acuerdo a alguno parametro especificado

-- Devuelve el promedio de salario por departamento 
SELECT department_id, AVG(salary) 
FROM employees 
GROUP BY department_id 
ORDER BY department_id;

-- Devuelve el maximo salario por departamento
SELECT department_id, MAX(salary)
FROM employees 
GROUP BY department_id

--Devuelve el numero de paises que hay en cada region
--(La tabla wf_contries no existe por eso no se pudo revisar la consulta)
SELECT COUNT(country_name), region_id 
FROM wf_countries 
GROUP BY region_id 
ORDER BY region_id

--Devuelve el maximo salario por departamento, excluyendo a King

SELECT department_id, MAX(salary) 
FROM employees 
WHERE last_name != 'King' 
GROUP BY department_id;

--Devuelve el numero de trabajadores que hay por job_id y su departamento 
SELECT department_id, job_id, count(*) 
FROM employees 
WHERE department_id > 40 
GROUP BY department_id, job_id;

--Devuelve el maximo salio por departamento
-- y solo devuelve los departamentos que tengan mas de un registro
SELECT department_id,MAX(salary), COUNT(*) 
FROM employees 
GROUP BY department_id
HAVING department_id > 10
ORDER BY department_id;

--ROLLUP genera totales y subtotales
-- Devuelve totales y subtateles de salarios por departamento
SELECT department_id, job_id, SUM(salary) 
FROM employees 
WHERE department_id < 50 
GROUP BY ROLLUP (department_id, job_id)

--CUBE--
-- Devuelve los totales y subtotales de todas las posibles combinaciones
-- de acuerdo a las agrupaciones
SELECT department_id, job_id, SUM(salary) 
FROM employees
WHERE department_id < 50 
GROUP BY CUBE (department_id, job_id)

--Grouping sets
--permite hacer una sola busqueda utilizando varias agrupaciones
SELECT department_id, job_id, manager_id, SUM(salary) 
FROM employees 
WHERE department_id < 50 
GROUP BY GROUPING SETS ((job_id, manager_id), (department_id, job_id), (department_id, manager_id));

--GROUPING
--
SELECT department_id, job_id, SUM(salary), 
GROUPING(department_id) AS "Dept sub total", 
GROUPING(job_id) AS "Job sub total"
FROM employees WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);

--UNION--
--Devuleve los datos de table employeer y de job history en una sola tabla 
SELECT hire_date, employee_id, job_id 
FROM employees 
UNION 
SELECT TO_DATE(NULL),employee_id, job_id 
FROM job_history
ORDER BY employee_id

--QUERY ANIDADO
-- hace una busqueda inicial y esa respuesta nos ayuda a hacer la busqueda siguiente
--primero buscamos la fecha de contratacion de vergas y de esa fecha nos
--regresa todos los empleados contratados despues de esta fecha
SELECT first_name, last_name, hire_date 
FROM employees 
WHERE hire_date > (SELECT hire_date FROM employees
WHERE last_name = 'Vargas');


--SUBQUERY QUE PERMITE BUSQUEDA EN DIFERENTES TABLAS
SELECT last_name, job_id, department_id 
FROM employees 
WHERE department_id = (SELECT department_id 
FROM departments 
WHERE department_name = 'Marketing')
ORDER BY job_id; 

SELECT last_name, job_id, salary, department_id 
FROM employees 
WHERE job_id = 
(SELECT job_id 
FROM employees 
WHERE employee_id = 141)
AND department_id = 
(SELECT department_id
FROM departments 
WHERE location_id = 1500);

--EJEMPLO DEVUELVE LOS EMPLEADOS QUE GANAN MENOS QUE EL SALARIO PROMEDIO
SELECT last_name, salary 
FROM employees
WHERE salary < 
(SELECT AVG(salary) 
FROM employees);

--Obtiene el salario minimo de cada departamento 
--mayor a salario minimo del departamento 50
SELECT department_id, MIN(salary)
FROM employees 
GROUP BY department_id 
HAVING MIN(salary) >
(SELECT MIN(salary) 
FROM employees 
WHERE department_id = 50)
oRDER BY department_id;

--
SELECT first_name, last_name 
FROM employees 
WHERE salary IN 
(SELECT salary 
FROM employees 
WHERE department_id = 20)


SELECT last_name, hire_date 
FROM employees 
WHERE EXTRACT(YEAR FROM hire_date) < ANY
(SELECT EXTRACT(YEAR FROM hire_date) 
FROM employees WHERE department_id=90)


SELECT department_id, MIN(salary)
FROM employees 
GROUP BY department_id 
HAVING MIN(salary) < ANY (SELECT salary 
FROM employees
WHERE department_id IN (10,20)) 
ORDER BY department_id;
--Esta consulta y la siguiente es lo mismo, solo es mas amplio
SELECT employee_id, manager_id, department_id 
FROM employees 
WHERE(manager_id,department_id) 
IN (SELECT manager_id,department_id 
FROM   employees
WHERE  employee_id IN (149,174)) 
AND employee_id NOT IN (149,174)

SELECT  employee_id, manager_id, department_id 
FROM    employees 
WHERE  manager_id IN 
    (SELECT  manager_id
    FROM    employees
    WHERE   employee_id IN          
    (149,174))
AND    department_id IN
    (SELECT  department_id 
    FROM    employees 
    WHERE   employee_id IN 
    (149,174))
AND  employee_id NOT IN(149,174);

------------------------------------------------
--Devuelve los empleados que ganan mas que el promedio de su departartamento
SELECT  o.department_id , o.first_name, o.last_name, o.salary
FROM employees o 
WHERE o.salary >
(SELECT AVG(i.salary) 
FROM employees i 
WHERE i.department_id = o.department_id)
Order by o.department_id;


WITH managers AS 
(SELECT DISTINCT manager_id
FROM employees 
WHERE manager_id 
IS NOT NULL)
SELECT last_name AS "Not a manager" 
FROM employees 
WHERE employee_id NOT IN 
(SELECT * FROM managers);
